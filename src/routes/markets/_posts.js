// Ordinarily, you'd generate this data from markdown files in your
// repo, or fetch them from a database of some kind. But in order to
// avoid unnecessary dependencies in the starter template, and in the
// service of obviousness, we're just going to leave it here.

// This file is called `_posts.js` rather than `posts.js`, because
// we don't want to create an `/blog/posts` route — the leading
// underscore tells Sapper not to do that.


const posts = [
    // {
    //   "address": "KT1AqJPj1ZvzvFXtw3F17oSTo5Hi9E66Q92N",
    //   "target_price": "111",
    //   "asset": "BTC-USD",
    //   "bidding_end": 1615676400000,
    //   "trading_end": 1615762800000,
    //   "phase": "trading",
    //   "ls_ratio": "0.45 / 0.55",
    //   "total_bids": "12221",
    //   "total_long_bids": "5555",
    //   "total_short_bids": "6666"
    // },
    // {
    //   "address": "KT192vhzxe6bH9tTMXjvbzf4UFLfwcZ9KFPQ",
    //   "target_price": "111",
    //   "asset": "BTC-USD",
    //   "bidding_end": 1615676400000,
    //   "trading_end": 1615762800000,
    //   "phase": "trading",
    //   "ls_ratio": "0.45 / 0.55",
    //   "total_bids": "1221",
    //   "total_long_bids": "555",
    //   "total_short_bids": "666"
    // },
    // {
    //   "address": "KT1K3ejz7wTDg4BpXk3zDUJuehNd6ohEqe8V",
    //   "target_price": "36000000000",
    //   "asset": "BTC-USD",
    //   "bidding_end": 1612036477000,
    //   "trading_end": 1612040077000,
    //   "phase": "maturity",
    //   "ls_ratio": "0.46 / 0.54",
    //   "total_bids": "26800000000000000000",
    //   "total_long_bids": "12300000000000000000",
    //   "total_short_bids": "14500000000000000000"
    // },
    // {
    //   "address": "KT1UGoRki6JEkPKC9qo3xciRb9f3emtwBkqQ",
    //   "target_price": "36000000000",
    //   "asset": "BTC-USD",
    //   "bidding_end": 1612036477000,
    //   "trading_end": 1612040077000,
    //   "phase": "maturity",
    //   "ls_ratio": "0.46 / 0.54",
    //   "total_bids": "26800000000000000000",
    //   "total_long_bids": "12300000000000000000",
    //   "total_short_bids": "14500000000000000000"
    // }
];

export default posts;
