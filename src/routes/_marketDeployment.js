import { DEPLOYER_ADDR } from './_constants';
export const marketDeployment = (
    long_bid,
    short_bid,
    cancel_fee_multiplier,
    exercise_fee_multiplier,
    min_capital,
    skew_limit,
    asset,
    target_price,
    bidding_end,
    trading_end
  ) => 
({
  kind: "transaction",
  amount: "0",
  destination: DEPLOYER_ADDR,
  parameters: {
    entrypoint: "deploy",
    value: {
      prim: "Pair",
      args: [
        {
          prim: "Pair",
          args: [
            {
              int: long_bid,
            },
            {
              int: short_bid,
            },
          ],
        },
        {
          prim: "Pair",
          args: [
            {
              prim: "Pair",
              args: [
                {
                  prim: "Pair",
                  args: [
                    {
                      int: cancel_fee_multiplier,
                    },
                    {
                      int: exercise_fee_multiplier,
                    },
                  ],
                },
                {
                  prim: "Pair",
                  args: [
                    {
                      int: min_capital,
                    },
                    {
                      int: skew_limit,
                    },
                  ],
                },
              ],
            },
            {
              prim: "Pair",
              args: [
                {
                  prim: "Pair",
                  args: [
                    {
                      string: asset,
                    },
                    {
                      int: target_price,
                    },
                  ],
                },
                {
                  prim: "Pair",
                  args: [
                    {
                      int: bidding_end,
                    },
                    {
                      int: trading_end,
                    },
                  ],
                },
              ],
            },
          ],
        },
      ],
    },
  },
});