import posts from './markets/_posts.js';

const contents = JSON.stringify(posts.map(post => {
	return {
		title: post.title,
		address: post.address
	};
}));

export function get(req, res) {
	res.writeHead(200, {
		'Content-Type': 'application/json'
	});

	res.end(contents);
}